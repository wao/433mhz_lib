/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Yet another HOLTEK decoder - this time for WHA1 and WHA2 alarm PIR (and presumably other related peripherals), which are based on a Holtek microcontroller in the same family, but running at 1MHz, with bespoke firmware.
// The structure should allow you do decode pretty much any 433MHz bitstream by expanding the state machine

//
// Based on http://www.holtek.com/pdf/Encoder_Decoder/HT6P2x5Av100.pdf
//
// Holtek ASK 300 -450MHz band Tx family decoder
//
// According to the HT6P2x54 datasheet, a message consists of pilot of 23 clocks,
// followed by "1/3 bit" i.e. one clock transition from the low to high to low, however watching the output of the WHA2 PIR on the scope suggested this device was different.
//
// The above datasheets shows this as 23 clocks of logic low... so our message should start with "silence" for ...
// 23 x clock wavelength (λ) + one high of "1/3 bit" - i.e. one clock length(λ)) consisting of from low to high to low transition.
//
// From the datasheet this is described thus...
//
// Section 1.4  - Synchronous Bit Waveform For the HT6P2x5A device, a synchronous bit waveform is 8-bits long.
//                It exhibits a low pulse for 23λ followed by a high pulse for 1λ ...
//
// The "Syncronous bit" is followed by address bits A0 - A19 (60 clocks (60 x λ since we have 3 clocks per bit))
// This is then followed by D3 - D0 (12 clocks)
// This in turn is followed by a 4 bit "anti-code" in other words the bits "0101" (a further 12 λ at 3λ per bit)
//
// Message frame is therefore 23+1+(20*3)+(4*3)+(4*3) clocks long i.e. 108 clocks
//
// Every message bit consists of a low to high transition, followed by a high to low transition, so we expect 2 (CHANGE) interrupts per bit.
//
// Every message frame therefore consists of 2 transistions (start bit) + 40 (address bit transitions) + 8 (data bit transitions) + 8 (anticode transitions for the 4 bits of 1010)
//
// Every valid message frame is 58 transitions long, and lasts for 108 clocks.
//
// Bit “1” consists of a “low” pulse for 2λ then changes to a “high” pulse for 1λ.
// Bit “0” consists of a “low” pulse for 1λ then changes to a “high” pulse for 2λ.
//
// Our state machine therefore needs to have the following states.
//
// 0 - Listening for start bit
//
// 1 - Looking for the pilot or "synchronous bit" - a low period (our start bit), followed by a high transition (at the end of the start bit), followed by a low transition (the end of the sync bit), where the low period (start bit) is >23 times longer
//     than the high period (sync bit). This high period is by definition (λ)
//
// 2 - Looking for the next 28 bits (20 message + 4 address + 4 "anticode" bits)  taking in 56  transitions (each bit is low to high, then high to low since it is the length
//     rather than the level which determines whether the bit is a 0 or a 1
//
//     This state should last for 84 clock cycles or 84(λ) +/- any clock drift.
//
// 3 - Decoding the address and data
//
// and so back to 1
//
//
//
// Icing on this particular cake is the ability to consistently derive the clock from state 1 thus rendering the code, clock agnostic, since there are
// a number of different members of this decoder family with different clock arrangements, varying from 1MHz resonators to 1KHz R/C or internal oscillators.
// More icing is the ability to allow for different message formats.
//
// Althoug all of the manufacturer declared variants use a 24 bit message, this particlar device appears to send 31 bits, there may be other variants
// that work differently. There are application notes that declare "encode 24 or 28 bits of address and data", so other Holtek in house variations also exist.
//

static unsigned int  radio_bit_end = micros();                             // These three variable will be updated by each radio_rx_pinchange()
static unsigned int  radio_bit_start = radio_bit_end;                      // no time has passed yet
static unsigned int  radio_bit_length = radio_bit_end - radio_bit_start;   //
static unsigned int  radio_first_semi_bit_length = 0;                      // First "half" bit length
static byte          radio_bit_mark_space_ratio = 3;                       //

static bool          radio_bit_processed  = false ;                        // Handshake flag beteen interrupt and state machine false when we have a valid bit waiting, true when it has been dealt with

static byte          radio_state_machine_state = 0;                        // The default state machine state
static unsigned int  radio_bit_zero_length = 0;                            // We need to magic these from the data stream
static unsigned int  radio_bit_one_length = 0;                             //
static unsigned int  radio_source_clock_period = 0;                        // We should be able to derive this from the stop bit after the "synchro bit"

const unsigned long   radio_start_bit_length_min = 10000 ;                  // Minimum length for low start pulse before sync bit (micro seconds)
//                                                                          // Experiments showed this to be around 11650 uS
const unsigned long   radio_start_bit_length_max = radio_start_bit_length_min + (radio_start_bit_length_min / 4) ; // Careful here.. this affects sensitivity
//                                                                          // Suggested maximum is 150% of minimum

const unsigned long   radio_start_sync_bit_ratio = radio_start_bit_length_min / 100; // Ratio of start bit to sync bit... for our chip, this looks to be approx 100...

// The HT6P2x5A data sheet states a ratio of 23 to 1, in the real world, the shape of pulse means it will be slightly higher, say 24 but not 100, as per
// testing with a PIR  ... looks like this is the first difference in the protocols.


const  unsigned long  radio_packet_length = 30;                              // Number of bits in the packet MSB bits as Address, LSB bits as data and "anticode"
static unsigned long  radio_packet_bitcount = 0;                             // Count of valid bits in this packet
static unsigned long  radio_packet  = 0;

// Slight hack..  we need to bitshift by one, but the default "1" is an int, so we need to define a 64 bit 1 otherwise the bitshift fails at 32 bits.
// I blame the compiler ;¬)
static unsigned long  bit_shift_long_one = 1;

// The result for my random device, in this case a WHA2 PIR
// TODO: Add a "learn" mode for up to 10 devices as per the WHA1 alarm.
// You can of course manually add as many as you like.. 
const unsigned long   device1 = 271660324;                                    // Address of one of my PIRs, change this to whatever you discover
const unsigned long   device2 = 2;                                            // Address of other devices
const unsigned long   device3 = 3;
const unsigned long   device4 = 4;
const unsigned long   device5 = 5;
const unsigned long   device6 = 6;
const unsigned long   device7 = 7;
const unsigned long   device8 = 8;
const unsigned long   device9 = 9;
const unsigned long   device10 = 10;


static unsigned int  radio_sync_bit_length = radio_start_bit_length_min / radio_start_sync_bit_ratio;  // First approximation of sync pulse length


// Arduino pin defs.
// Radio Pin
static byte radio_pin = 2;
static bool radio_pin_state, previous_radio_pin_state = 0;

// Define LED Pin, used by toggle_led() toggle_led_on() and toggle_led_off()
static byte led_pin = 13;
// set flash_led to false to leave the LED off
static boolean led_enabled = true;
static boolean led_on = false;
static long led_duration = 0;
static long led_off_time = 0;

void setup() {
  // Set the serial console speed
  Serial.begin(115200);

  // Define the radio pin
  pinMode(radio_pin, INPUT);
  //  pinMode(radio_pin, INPUT_PULLUP);

  // Define the LED pin
  pinMode(led_pin, OUTPUT);

  // Setup an interrupt to trigger every time the radio pin changes state.
  attachInterrupt(0, radio_rx_pinchange, CHANGE);

  //Put a 1 in the start bit position
   radio_packet |= 1 << (bit_shift_long_one + radio_packet_length);

}

void loop() {

  if ((led_on))
  {
    if (millis() > led_off_time )
    {
      led_on = false;
      led_off();
    }
  }

  // State machine to find data packet.

  // Start of state machine
  // First we check for a new radio "bit" (low or high pulse)

  if (!radio_bit_processed) {
    /// Mask the interrup and grab the bit details
    detachInterrupt(0);
    unsigned long this_radio_bit_length = radio_bit_length ;
    bool this_radio_pin_state = !radio_pin_state;
    unsigned long this_previous_radio_pin_state = previous_radio_pin_state;
    // and unmask the interrupt
    attachInterrupt(0, radio_rx_pinchange, CHANGE);

    // Switch to operate at current state
    switch (radio_state_machine_state) {
      case 0 :
        // State 0 - checking for long sync pulse
        // We need a bit within our time frame, and set low, to move to state 1 otherwise we go back to state 0 (NOTE: we "and" with !this_radio_pin_state to and with a 1)
        if ((this_radio_bit_length > radio_start_bit_length_min) and (this_radio_bit_length < radio_start_bit_length_max) and (!this_radio_pin_state))
        {
          radio_sync_bit_length = this_radio_bit_length / radio_start_sync_bit_ratio; // Sync pulse should be no shorter than this theoretically
          radio_state_machine_state = 1;
          // DEBUG:
          // Uncomment these debug sections to "see" if you are getting Long Sync,  Start Pulse etc...
          // but bear in mind any serial output will affect the over all timing, so take care.. to much chat breaks the state machine or looses bits.
          // keep debug messages short

          //Serial.print("Start=");
          //Serial.print(this_radio_pin_state);
          //Serial.print(",");
          //Serial.print(this_radio_bit_length);
          //Serial.print(",");
          //Serial.print(radio_sync_bit_length);
          // Serial.println("us.");
          //
        } else {
          if (this_radio_bit_length > radio_start_bit_length_min)
          {
            // DEBUG:
            //  Serial.println(this_radio_bit_length);
          }
        }

        break;
      case 1 :
        // DEBUG:
        // State 1 checking for short "sync" bit(λ)
        // Serial.print("Sync=");
        // Serial.println(this_radio_bit_length);
        // Serial.print("Guess=");
        // Serial.println(radio_sync_bit_length);
        //
        if ((this_radio_bit_length - radio_sync_bit_length) < 1000) // If within 25% of our guess...
        {
          // DEBUG:
          //Serial.println("S");
          //
          radio_state_machine_state = 2;
        } else {
          // DEBUG:
          // Serial.print((this_radio_bit_length - radio_sync_bit_length));
          // Serial.println("!S");
          // State 1 checking for short "sync" bit(λ)
          //Serial.print("Sync=");
          //Serial.println(this_radio_bit_length);
          //Serial.print("Guess=");
          //Serial.println(radio_sync_bit_length);
          //
          radio_state_machine_state = 0;
          break;
        }
        break;
      case 2 :
        // DEBUG:
        // State 2 checking for start of bit
        // Serial.print("Start=");
        // Serial.println(this_radio_bit_length);
        //
        radio_first_semi_bit_length = this_radio_bit_length;
        radio_state_machine_state = 3;
        break;
      case 3 :
        //DEBUG:
        // Short Long is a '0' and Long Short is a 1
        //
        // DEBUG: watch the bits as they are discovered
        /*
        if (radio_first_semi_bit_length >  (this_radio_bit_length * radio_bit_mark_space_ratio))
        {
          Serial.print("1");
        } else {
          Serial.print("0");
        }
        */
        //
        // Serial.print(radio_first_semi_bit_length);Serial.print(",");
        // Serial.println(this_radio_bit_length);
        // if its a 0 then we simply bitshift left, otherwise we bitshift left and add one
        //radio_packet <<= 1;
        if (radio_first_semi_bit_length > (this_radio_bit_length * radio_bit_mark_space_ratio) )
        {
          // Set bit

          radio_packet |= (bit_shift_long_one << (radio_packet_length - radio_packet_bitcount));
          // Serial.println(radio_packet_bitcount);
          // radio_packet &= ~(1 << radio_packet_bitcount);
        }
        //
        radio_packet_bitcount++;
        if ( radio_packet_bitcount <= radio_packet_length )
        {
          radio_state_machine_state = 2;
          break;
        } else {
          //DEBUG
          /*
          Serial.println(radio_packet_bitcount);
          Serial.println(radio_first_semi_bit_length);
          Serial.println(this_radio_bit_length);
          Serial.print("Packet=");
          //
          //Serial.println(radio_packet, BIN);
          //Serial.println(radio_packet);
          //Serial.println("..........");
          */
          switch (radio_packet) {
            // I placed a switch - case statement here in case we want to perform different actions depending on what was triggered. 
            case device1 :
              // Serial.println();
              Serial.println("Device 1");
              led_blink(100);
              break;
            case device2 :
              // Serial.println();
              Serial.println("Device 2");
              led_blink(100);
              break;
            case device3 :
              // Serial.println();
              Serial.println("Device 3");
              led_blink(100);
              break;
            case device4 :
              // Serial.println();
              Serial.println("Device 4");
              led_blink(100);
              break;
            case device5 :
              // Serial.println();
              Serial.println("Device 5");
              led_blink(100);
              break;
            case device6 :
              // Serial.println();
              Serial.println("Device 6");
              led_blink(100);
              break;
            case device7 :
              // Serial.println();
              Serial.println("Device 7");
              led_blink(100);
              break;
            case device8 :
              // Serial.println();
              Serial.println("Device 8");
              led_blink(100);
              break;
            case device9 :
              // Serial.println();
              Serial.println("Device 9");
              led_blink(100);
              break;
            case device10 :
              // Serial.println();
              Serial.println("Device 10");
              led_blink(100);
              break;
            default :
              // DEBUG: uncomment these lines to see what unknown devices are detected.
              //Serial.println();
              //Serial.println("Unknown Device");
              //Serial.println(radio_packet, HEX);
              //Serial.println(radio_packet);
              // Serial.println(radio_packet_bitcount);
              //Serial.println(radio_packet);
              led_blink(10);
              break;
          }

          //
          // DEBUG:
          // Serial.println();
          //
          radio_packet = 0 ;
          radio_packet |= 1 << (bit_shift_long_one + radio_packet_length);
          radio_packet_bitcount = 0;
          // Output result and go back to state 0
          radio_state_machine_state = 0;
          break;
        }
      default:
        // Serial.println(radio_state_machine_state);
        radio_state_machine_state = 0;
        break;
    }
    radio_bit_processed = true;
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Toggle the LED if enabled -
//  Note: Careless use of the LED routines may also affect the state machine timing...
/////////////////////////////////////////////////////////////////////////////////////////////////////////
void led_blink ( long led_duration ) {
  if (led_enabled)
  {
    led_on = true;
    led_off_time = millis() + led_duration;
    digitalWrite(led_pin, !digitalRead(led_pin));
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Toggle the LED off if enabled
/////////////////////////////////////////////////////////////////////////////////////////////////////////
void led_off () {
  if (led_enabled)
  {
    digitalWrite(led_pin, 0);
  }
}

void radio_rx_pinchange() {
  // Rather obviously each time we hit this code, the radio pin will have just inverted relative to the last time we were called.
  // We are interested in the time between calls (the length of the "bit") which is the time in the previous state.
  radio_pin_state = digitalRead(radio_pin);
  radio_bit_start = radio_bit_end;
  // Using an unsigned int for timings will keep our times within 0xFFFF clicks, we dont need huge times here.
  // Besides 16 bit unsigned maths should be quicker.
  radio_bit_end = micros();
  radio_bit_length = radio_bit_end - radio_bit_start ;
  radio_bit_processed = false;
}
